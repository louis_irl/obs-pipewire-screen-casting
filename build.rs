extern crate bindgen;

use std::collections::HashSet;
use std::env;
use std::path::PathBuf;

use dbus_codegen;

fn main() {
    println!("cargo:rerun-if-changed=build.rs");

    // Tell cargo to tell rustc to link the system obs
    // shared library.
    println!("cargo:rustc-link-lib=obs");

    let ignored_macros = IgnoreMacros(
            vec![
                "FP_INFINITE".into(),
                "FP_NAN".into(),
                "FP_NORMAL".into(),
                "FP_SUBNORMAL".into(),
                "FP_ZERO".into(),
                "IPPORT_RESERVED".into(),
            ]
            .into_iter()
            .collect(),
    );

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("src/c/obs_wrapper.h")
        .emit_builtins()
        .ctypes_prefix("libc")
        .raw_line("extern crate libc;")
        .blacklist_type("max_align_t")
        .derive_default(true)
        .parse_callbacks(Box::new(ignored_macros))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");

    generate_binding("src/xml/org.freedesktop.portal.ScreenCast.xml", "screen_cast.rs");
    generate_binding("src/xml/org.freedesktop.portal.Request.xml", "request.rs");
}

fn generate_binding(src: &str, dest: &str) -> () {
    println!("cargo:rerun-if-changed={}", src);

    let out_dir = env::var("OUT_DIR").unwrap();
    let dest_path = std::path::Path::new(&out_dir).join(dest);
    let gen_opts = dbus_codegen::GenOpts {
        methodtype: None,
        skipprefix: Some("org.freedesktop.portal".to_string()),
        ..Default::default()
    };
    let content = std::fs::read_to_string(src).unwrap();

    let generated = dbus_codegen::generate(&content, &gen_opts).unwrap();

    std::fs::write(dest_path, generated).unwrap();
}

#[derive(Debug)]
struct IgnoreMacros(HashSet<String>);

impl bindgen::callbacks::ParseCallbacks for IgnoreMacros {
    fn will_parse_macro(&self, name: &str) -> bindgen::callbacks::MacroParsingBehavior {
        if self.0.contains(name) {
            bindgen::callbacks::MacroParsingBehavior::Ignore
        } else {
            bindgen::callbacks::MacroParsingBehavior::Default
        }
    }
}
