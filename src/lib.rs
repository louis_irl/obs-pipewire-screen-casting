#[macro_use]
mod obsmodule;

mod libobs;

mod screen_cast;
mod portal;
mod pipewire;
mod screen_cast_source;

obs_declare_module!("obs-pipewire-screen-casting", "Desktop/GNOME screen casting via Pipewire");

obs_module_author!("Pete Johanson");

#[no_mangle]
pub extern fn obs_module_load() -> bool {
    if let Err(_) = gstreamer::init() { return false; }

    let info = screen_cast_source::get_source_info();
    
    unsafe {
        libobs::obs_register_source_s(&info, std::mem::size_of::<libobs::obs_source_info>());
    }

    true
}

#[no_mangle]
pub extern fn obs_module_unload() -> () {
}
