#![allow(dead_code)]

pub mod screen_cast {
    include!(concat!(env!("OUT_DIR"), "/screen_cast.rs"));
}

pub mod request {
    include!(concat!(env!("OUT_DIR"), "/request.rs"));
}