# Open Broadcaster Software PipeWire Screen Casting Plugin

[![pipeline status](https://gitlab.com/petejohanson/obs-pipewire-screen-casting/badges/master/pipeline.svg)](https://gitlab.com/petejohanson/obs-pipewire-screen-casting/commits/master)

## About

Starting with GNOME 3.20, a solution for screen casting and remote desktop session has been introduced, leveraging PipeWire for
streaming. This offers a solution that used to only be possible on X11. The same API will/is used by Flatpack for controlling
requests/access to the same functionality.

This plugins aims to add screen casting support to [Open Broadcaster Software](https://obsproject.com/) using that API.

## Status

At this point, this plugin should be considered a proof of concept only. It is *not* yet used for any production
streaming with OBS. If it breaks, you get to keep both halves.

## Downloads

There are build jobs for Fedora 29 and Debian Testing of the plugin, which may or may not actually be usable on
your current system:

* [Fedora 29 Plugin](https://gitlab.com/petejohanson/obs-pipewire-screen-casting/-/jobs/artifacts/master/raw/target/release/libobs_pipewire_screen_casting.so?job=build:fedora:29)
* [Debian Testing Plugin](https://gitlab.com/petejohanson/obs-pipewire-screen-casting/-/jobs/artifacts/master/raw/target/release/libobs_pipewire_screen_casting.so?job=build:debian:testing)

## Installation Instructions

1. Download the file `libobs_pipewire_screen_casting.so` for the appropriate distro from the links above.
1. Copy the file into your OBS Studio plugin directory, e.g. `sudo cp libobs_pipewire_screen_casting.so /usr/lib64/obs-plugins/`
1. Run OBS Studio
1. Add a new source, and select `PipeWire Screen Casting` from the list.
1. When prompted, select the display you would like to share, and click "Share".

## Caveats

1. If using scaling for HiDPI screens, there's a recently fixed [Mutter bug](https://gitlab.gnome.org/GNOME/mutter/issues/449) that you might need to manually apply a fix for.